/************************************************************************/
/*
线程通信
主线程等待用户输入，产生不同的消息，并把这些消息post给子线程，子线程根据
产生的消息作出不同的反应，这些线程可以是工作线程，也可以是UI线程
*/
/************************************************************************/
#include <windows.h>
#include <stdio.h>
#include <malloc.h>
#include <process.h>

#define MY_MSG WM_USER+100
const int MAX_INFO_SIZE = 20;

HANDLE hStartEvent;// thread start event

// thread function
// 接收线程函数
unsigned int __stdcall fun(void *param) {
	printf("thread fun start\n");

	MSG msg;
	// 创建消息队列
	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
	if (!SetEvent(hStartEvent)) // set thread start event
	{
		printf("set start event failed,errno:%d\n", GetLastError());
		return 1;
	}
	char *pInfo = NULL;
	while (true)
	{
		if (GetMessage(&msg,0,0,0)) // get message from message queue
		{
			switch (msg.message)
			{
			case MY_MSG:
				pInfo = (char *)msg.wParam;
				printf("recv %s\n", pInfo);
				break;
			default:
				break;
			}
		}
	}
	return 0;
}

int main() {
	HANDLE hThread;
	unsigned int nThreadId;

	hStartEvent = CreateEvent(0, FALSE, FALSE, 0);// create thread start event
	if (hStartEvent == 0)
	{
		printf("create start event failed,errno:%d\n", GetLastError());
		return 1;
	}

	// start thread
	hThread = CreateThread(NULL, 0, &fun, NULL, 0, &nThreadId);
	if (hThread == 0)
	{
		printf("start thread failed,errno:%d\n", GetLastError());
		return 1;
	}

	// Wait thread start event to avoid PostThreadMessage return errno:1444
	WaitForSingleObject(hStartEvent, INFINITE);
	CloseHandle(hStartEvent);

	int count = 0;
	while (true)
	{
		char *pInfo = (char *)malloc(sizeof(char) * MAX_INFO_SIZE);// create dynamic msg
		//sprintf(pInfo, "msg_%d", ++count);
		sprintf_s(pInfo, MAX_INFO_SIZE, "msg_%d", ++count);
		if (!PostThreadMessage(nThreadId,MY_MSG,(WPARAM)pInfo,0)) // 发送线程
		{
			printf("post message failed,errno:%d\n", GetLastError());
			free(pInfo);
		}
		Sleep(1000);
	}

	CloseHandle(hThread);
	return 0;
}